class Mapa {

    miVisor;
    mapaBase;
    posicionInicial;
    escalaInicial;
    proveedorURL;
    atributosProveedor;
    marcadores = [];
    circulos = [];
    poligonos = [];

    constructor() {

        this.posicionInicial = [4.628549, -74.172523];
        this.escalaInicial = 14;
        this.proveedorURL = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
        this.atributosProveedor = {
            maxZoom: 20
        };


        this.miVisor = L.map("mapid");
        this.miVisor.setView(this.posicionInicial, this.escalaInicial);
        this.mapaBase = L.tileLayer(this.proveedorURL, this.atributosProveedor);
        this.mapaBase.addTo(this.miVisor);
    }


    colocarMarcador(posicion) {

        this.marcadores.push(L.marker(posicion));
        this.marcadores[this.marcadores.length - 1].addTo(this.miVisor);
    }

    colocarCirculo(posicion, configuracion) {

        this.circulos.push(L.circle(posicion, configuracion));
        this.circulos[this.circulos.length - 1].addTo(this.miVisor);

    }

    colocarPoligono(posicion) {

        this.poligonos.push(L.polygon(posicion));
        this.poligonos.addTo(this.miVisor);
    }

}

let miMapa = new Mapa();

miMapa.colocarMarcador([4.628549, -74.172523]);
miMapa.colocarCirculo([4.628549, -74.172523], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 500
});

miMapa.colocarMarcador([4.628025, -74.169399]);

miMapa.colocarCirculo([4.628025, -74.169399], {
    color: 'blue',
    fillColor: 'blue',
    fillOpacity: 0.5,
    radius: 500
});

miMapa.colocarPoligono([
    [4.625105231763941, -74.1690444946289],
    [4.619287726754987, -74.16123390197754],
    [4.626816253557569, -74.15505409240723],
    [4.6321203948321275, -74.16406631469727],
    [4.625105231763941, -74.1690444946289]
]);
